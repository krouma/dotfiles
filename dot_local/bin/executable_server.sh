echo
echo [1] FTB Infinity Evolved Skyblock
echo [2] Herobrine\'s Return
echo [3] FTB Revelation 2.7.0++
echo [4] Omnifactory
echo [5] LineageOTA
echo
echo -n "Choose wisely: "
read choice
echo
if [ $choice -eq 1 ]; then
	/home/krouma/.local/opt/ftb/FTBIES:E/ServerStart.sh
fi
if [ $choice -eq 2 ]; then
	/home/krouma/.local/opt/Minecraft/server_1.12.2/ServerStart.sh
fi
if [ $choice -eq 3 ]; then
	/home/krouma/Games/minecraft/servers/FTBRevelationServer_2.7.0/ServerStart.sh
fi
if [ $choice -eq 4 ]; then
	/home/krouma/Games/minecraft/servers/Omnifactory-v1.2.2+-server/launch.sh
fi
if [ $choice -eq 5 ]; then
	cd /home/krouma/Projekty/LineageOTA
	docker run \
	-d \
	-p 80:80 \
	-v "/home/krouma/Projekty/LineageOTA/builds:/var/www/html/builds/full:z" \
	julianxhokaxhiu/lineageota
fi
