#!/usr/bin/bash

KEYS_PATH="/etc/refind.d/keys"
NVIDIA_DRIVER_VERSION=$(rpm -qa nvidia-driver | awk '{ print $3 }' FS='-' OFS='-')

COMMAND="${1}"
KERNEL_VERSION="${2}"

MODULES_PATH=/usr/lib/modules/$KERNEL_VERSION/extra

case "$COMMAND" in
    add)
        for MODULE in nvidia nvidia-drm nvidia-uvm nvidia-modeset
        do
            xz --decompress ${MODULES_PATH}/${MODULE}.ko.xz
            /usr/src/kernels/${KERNEL_VERSION}/scripts/sign-file sha256 ${KEYS_PATH}/refind_local.key ${KEYS_PATH}/refind_local.cer ${MODULES_PATH}/${MODULE}.ko
            xz --compress ${MODULES_PATH}/${MODULE}.ko
        done
        ;;
    *)
        ;;
esac

