#!/usr/bin/env bash

help()
{
    echo "Usage: powersave.sh [action] "
    echo "Actions:"
    echo "    help                              => shows this help"
    echo "    on                                => turn on powersaving"
    echo "    off                               => turn off powersaving"
}

ACTION="$1"
CORES=(2 3 4 5 8 9 10 11)
CORES4=(4 5 10 11)

case "$ACTION" in
    help | h | "")
        help
        ;;
    on)
        for CORE in ${CORES[@]}; do
            sudo sh -c "echo 0 > /sys/devices/system/cpu/cpu${CORE}/online"
    	done
	;;
    off)
	    for CORE in ${CORES[@]}; do
	        sudo sh -c "echo 1 > /sys/devices/system/cpu/cpu${CORE}/online"
	    done
	;;
	4-on)
        for CORE in ${CORES4[@]}; do
            sudo sh -c "echo 0 > /sys/devices/system/cpu/cpu${CORE}/online"
    	done
	;;
    4-off)
	    for CORE in ${CORES4[@]}; do
	        sudo sh -c "echo 1 > /sys/devices/system/cpu/cpu${CORE}/online"
	    done
	;;
esac

