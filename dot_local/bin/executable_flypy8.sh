#!/usr/bin/env bash

flake8 $*
mypy --strict --disallow-any-explicit $*

