cd ~/Projects/lineageos
source build/envsetup.sh
export USE_CCACHE=1
export CCACHE_COMPRESS=1
ccache -M 75G
export ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx12G"
export ANDROID_PRODUCT_OUT="out/target/product/nicki"
lunch lineage_nicki-userdebug
export LC_ALL=C
