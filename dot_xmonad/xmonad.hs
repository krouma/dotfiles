-- default desktop configuration for Fedora
-- edited by krouma
module Main where

import System.Posix.Env (getEnv)
import Data.Maybe (maybe)

import XMonad
import XMonad.Config.Desktop
import XMonad.Config.Gnome
import XMonad.Config.Kde
import XMonad.Config.Xfce

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.IfMax
import XMonad.Layout.NoBorders
import XMonad.Layout.Simplest
import XMonad.Layout.ToggleLayouts
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys,removeKeys)
import qualified XMonad.StackSet as W                       -- Switching between ws
import qualified Data.Map as M
import System.IO
import Graphics.X11.Types
import Graphics.X11.ExtraTypes.XorgDefault(xK_ecaron, xK_scaron, xK_ccaron, xK_rcaron, xK_zcaron)
import Graphics.X11.ExtraTypes.XF86
import XMonad.Prompt.Pass (passPrompt)
import XMonad.Prompt (XPConfig (searchPredicate, sorter), greenXPConfig, amberXPConfig)
import XMonad.Prompt.FuzzyMatch (fuzzyMatch, fuzzySort)

themeFgDarkerColor    = "#657b83"
themeBgColor          = "#eee8d5"
themeGreenColor       = "#759900"
themeRedColor         = "#dc322f"
themeBlueColor        = "#2aa198"
themeBrightBlueColor  = "#2aa1d8"

main = do
     session <- getEnv "DESKTOP_SESSION"
     let sessionConfig = maybe desktopConfig desktop session
     xmproc <- spawnPipe "/usr/bin/xmobar ~/.xmonad/xmobarrc"
     picom <- spawnPipe "picom --vsync --backend glx"
     xmonad $ ewmhFullscreen sessionConfig
         { modMask    = myModMask                          -- Rebind Mod to the Windows key
         , focusedBorderColor = themeFgDarkerColor
         , layoutHook = myLayoutHook
         , manageHook = manageHook sessionConfig <+> myManageHook
         , workspaces = myWorkspaces
         , logHook = dynamicLogWithPP xmobarPP              -- Hook for xmobar
             { ppOutput = hPutStrLn xmproc
             , ppTitle = xmobarColor themeGreenColor "" . shorten 192        -- formatting window title
             , ppCurrent = xmobarColor themeBrightBlueColor "" . wrap "[" "]"     -- formatting current ws
             }

         } `removeKeys` myRemoveKeys `additionalKeys` myKeys

myWorkspaces = [show n | n <- [1..9]] ++ ["F" ++ show n | n <- [1..12]]

myKeys = [ ((0, xF86XK_AudioLowerVolume ), spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
         , ((0, xF86XK_AudioRaiseVolume ), spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
         , ((shiftMask, xF86XK_AudioLowerVolume ), spawn "pactl set-source-volume @DEFAULT_SOURCE@ -5%")
         , ((shiftMask, xF86XK_AudioRaiseVolume ), spawn "pactl set-source-volume @DEFAULT_SOURCE@ +5%")
         , ((0, xF86XK_AudioMute), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
         , ((0, xF86XK_AudioPlay), spawn "playerctl play-pause")
         , ((0, xF86XK_AudioPrev), spawn "playerctl previous")
         , ((0, xF86XK_AudioNext), spawn "playerctl next")
         , ((0, xF86XK_MonBrightnessDown), spawn "brightnessctl -q set 5%-")
         , ((0, xF86XK_MonBrightnessUp), spawn "brightnessctl -q set +5%")
         , ((0, xF86XK_PowerOff), spawn "systemctl suspend")
         , ((0, xF86XK_Sleep), spawn "systemctl suspend")
         , ((0, xF86XK_WLAN), spawn "toggle_wifi.sh")
         , ((myModMask, xK_o), spawn "j4-dmenu-desktop")
         , ((myModMask .|. shiftMask, xK_o), spawn "ssh -X isengard sh -c \"j4-dmenu-desktop\"")
         , ((myModMask, xK_d), spawn "display.sh")
         , ((myModMask .|. controlMask, xK_p), passPrompt myXPConfig)
         ] ++
         [((m .|. myModMask, k), windows $ f i)                               -- switching ws
             | (i, k) <- zip
                 myWorkspaces
                 ([xK_plus, xK_ecaron, xK_scaron, xK_ccaron, xK_rcaron, xK_zcaron
                 ,xK_yacute, xK_aacute, xK_iacute]
                   ++ [xK_F1..xK_F12])
             , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
         ]

myRemoveKeys = [] -- [(myModMask, xK_p)]

myLayoutHook = avoidStruts $ toggleLayouts (noBorders Full) $ lessBorders Screen myLayouts

myLayouts = Tall 1 (3/100) (1/2)
            ||| Mirror (Tall 1 (3/100) (1/2))
            ||| Full

myManageHook = composeAll
         [ isDialog --> doCenterFloat
         , className =? "xdg-desktop-portal-gnome" --> doCenterFloat
         -- , className =? "Xfce-polkit"              --> doFloat
         , className =? "Xfce4-appfinder"          --> doCenterFloat
         , className =? "kdeconnect.daemon"        --> doFullFloat
         , stringProperty "WM_WINDOW_ROLE" =? "GtkFileChooserDialog" --> doCenterFloat
         ]

myModMask :: KeyMask
myModMask = mod4Mask

myXPConfig :: XPConfig
myXPConfig = def
             { searchPredicate = fuzzyMatch
             , sorter = fuzzySort
             }


desktop "gnome" = gnomeConfig
desktop "kde" = kde4Config
desktop "xfce" = xfceConfig
desktop "xmonad-mate" = gnomeConfig
desktop _ = desktopConfig

changeKeys :: XConfig a -> (KeyMask, KeySym) -> (KeyMask, KeySym) -> X () -> XConfig a
changeKeys conf orig new commNew = undefined
--    where commOrig = (keys conf) M.! orig
